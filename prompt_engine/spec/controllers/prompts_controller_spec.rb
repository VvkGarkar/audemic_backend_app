# require 'rails_helper'

# RSpec.describe PromptsController, type: :controller do
#   describe "GET #index" do
#     context "with search query" do
#       let(:query) { "Prompt 1" }
#       let(:found_prompts) { ["Prompt 1", "Prompt 2"] }

#       before do
#         # Stubbing Elasticsearch search method
#         allow(Prompt.__elasticsearch__).to receive(:search).with(query).and_return(
#           double(response: {
#             'hits' => {
#               'hits' => found_prompts.map { |content| { '_source' => { 'data' => content } } }
#             }
#           })
#         )
#       end

#       it "performs search and assigns prompts" do
#         get :index, params: { query: query }
#         expect(assigns(:prompts)).to eq(found_prompts)
#       end

#       it "creates an activity log when prompts are found" do
#         expect {
#           get :index, params: { query: query }
#         }.to change(ActivityLog, :count).by(1)

#         audit_log = ActivityLog.last
#         expect(audit_log.search_term).to eq(query)
#         expect(audit_log.content).to eq(found_prompts)
#       end
#     end

#     context "without search query" do
#       it "does not perform search and assigns empty prompts" do
#         get :index
#         expect(assigns(:prompts)).to be_nil
#       end

#       it "does not create an activity log" do
#         expect {
#           get :index
#         }.not_to change(ActivityLog, :count)
#       end
#     end
#   end
# end
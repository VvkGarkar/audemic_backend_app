Rails.application.routes.draw do
  get "/prompt/search", to: "prompts#index"

  root "prompts#index"
end
# Import prompts into Elasticsearch using bulk indexing
require 'elasticsearch/model'
require_relative '../lib/elasticsearch_client'
require 'net/http'
require 'json'

# Method to import bulk data into ElasticSearch
def bulk_prompt_import
  # Create or refresh the Elasticsearch index
  create_or_refresh_index
  # Fetch prompts data from an external source
  response = fetch_prompts_data

  if response.code == '200'
    # Parse JSON response
    data = JSON.parse(response.body)
    # Extract prompts from the response
    prompts = data['rows'].map { |row| row['row']['Prompt'] }
    # Import prompts into Elasticsearch
    import_prompts_to_elasticsearch(prompts)
  else
    # Handle failed HTTP request
    puts "HTTP request failed with status code: #{response.code}"
  end
end

# Method to create or refresh the Elasticsearch index
def create_or_refresh_index
  # Configure Elasticsearch client
  client = ElasticsearchClient.client

  # Index new Elasticsearch creation if not present
  index_name = 'prompts'
  client.indices.create(index: index_name) unless client.indices.exists(index: index_name)
end

# Method to fetch prompts data from an external source
def fetch_prompts_data
  # Constructing a URI to fetch prompts data from an external source (Hugging Face Datasets Server)
  uri = URI('https://datasets-server.huggingface.co/rows?dataset=Gustavosta%2FStable-Diffusion-Prompts&config=default&split=train&offset=0&length=100')
  # Sending a GET request to the constructed URI to retrieve prompts data
  Net::HTTP.get_response(uri)
end

# Method to import prompts into Elasticsearch using bulk indexing
def import_prompts_to_elasticsearch(prompts)
  client = ElasticsearchClient.client

  # Delete all existing prompts
  client.delete_by_query({
    index: 'prompts',
    body: {
      query: { match_all: {} }
    }
  })

  # Creating a bulk request body for indexing prompts in Elasticsearch using the Bulk API
  prompts_arr = prompts.map do |prompt|
    # Each prompt is represented as a pair of index metadata and data, separated by a newline character
    "#{({ index: { _index: 'prompts' } }).to_json}\n#{({ data: prompt }).to_json}\n"
  end

  bulk_request_body = prompts_arr.join

  # Send bulk request to index prompts in Elasticsearch
  response = client.bulk(body: bulk_request_body)

  # Handle successful bulk indexing
  if response['errors'] == false
    puts "[***Successfully #{prompts.size} prompts imported.***]"
  else
    puts "Bulk indexing errors: #{response['errors']}"
  end
end

# Calling method to import prompts bulk data into the ElasticSearch
bulk_prompt_import
# Module for managing the Elasticsearch client connection in a Rails application
module ElasticsearchClient
  # Method to retrieve an Elasticsearch client instance
  def self.client
    # Create a new Elasticsearch client instance with specified configurations
    Elasticsearch::Client.new(
      # Elasticsearch URL. Defaults to 'https://localhost:9200' if not provided in the environment
      url: ENV['ELASTICSEARCH_URL'] || 'https://localhost:9200',
      request_timeout: 5,

      # Transport options for the Elasticsearch client
      transport_options: {
        # SSL settings, with verification disabled for testing (not recommended in production)
        ssl: { verify: false },
        headers: { 'Content-Type' => 'application/json' },
        request: { timeout: 5 }
      },

      # Elasticsearch username for authentication
      user: 'elastic',
      password: ENV['ELASTIC_SEARCH_PASSWORD'],
      # Logger for Elasticsearch client, using the Rails logger
      logger: Rails.logger
    )
  end
end
# Project Name
Audemic Prompt Engine

## Description
The Audemic Prompt Engine project offers Database Seed functionality for Elasticsearch integration, Elasticsearch bulk indexing for prompt import, and a robust Search Controller for efficient querying. Local setup entails prerequisites like Ruby, Rails, and Docker, along with configuring environment variables. Users can effortlessly run and test the project locally, utilize Docker for a simplified setup, or access it directly via the deployed URL on Heroku.

**Audemic Prompt Engine Url-** https://audemic-app-new-48ec85db38a6.herokuapp.com

## Features
### Database Seed
This repository contains scripts for seeding data into Elasticsearch, ensuring a fresh and consistent dataset. The process involves removing existing data from Elasticsearch and populating it with a predefined dataset.
This seed functionality is designed for a seamless integration into your development and testing environments.

1. **File location:**
/audemic_backend_app/prompt_engine/db/seeds.rb
2. **Run Seed Script:**
rails db:seed

### ElasticSearch
This GitLab repository contains Elasticsearch for importing prompts into Elasticsearch using bulk indexing. The provided scripts utilize the Elasticsearch Ruby gem to interact with Elasticsearch, allowing you to efficiently manage and update your prompts dataset.

#### How Indexing Works
1. **Creating an Index:**
The script checks for the existence of the 'prompts' index and creates it if absent.

2. **Deleting Existing Data:**
Existing data in the 'prompts' index is removed before importing new prompts.

3. **Bulk Indexing:**
Prompts are imported using bulk indexing, where a structured request is sent to Elasticsearch for efficient processing.

* **Important Note:-** 
1. We import 100 records into Elasticsearch due to constraints in the Hugging Face API, which permits fetching only 100 records at a time. However, it's flexible to fetch a larger dataset if necessary.
2. Stop words filtering is not currently implemented in the provided script. To enhance the indexing process, consider adding functionality to filter stop words during prompt import. 

### Search Controller Functionality
* **Endpoint** - GET /prompt/search:
Handles search queries using Elasticsearch.

* **Query Parameter Handling:**
Assigns the search query parameter (query) to @search_query.

* **Response Parsing:**
Parses the Elasticsearch response into JSON format, extracting relevant data from the 'hits' array.

* **Error Handling:**
Handles various scenarios, displaying user-friendly alerts for no results, index not found, request timeouts, and generic errors.

* **Asynchronous Logging:**
Enqueues an asynchronous job (SearchActivityTrackerJob) for activity logging during searches.

* **Database Schema:**
This auto-generated schema defines the activity_logs table, logging search activities. It captures the search_keyword, an array of associated error/content, and timestamps for created_at and updated_at. The table facilitates efficient tracking of search-related events, with built-in error handling through timestamps.

**Note:** Use Active Record migrations for any schema modifications; avoid direct edits to the file.

## Local Setup

### Prerequisites
Ruby (version 3.1.3)
Rails (version 7.1.3.2)
Elasticsearch (version 7.2)
Docker

### Create .env File inside 'prompt_engine' directory

### .env
- AUDEMIC_PROMPT_ENGINE_DATABASE_PASSWORD=audemic123
- SECRET_KEY_BASE=89a3e9dc9925a7e37e09b96aa44f0869
- POSTGRES_USER=vivek
- POSTGRES_PASSWORD=12345
- ELASTICSEARCH_URL=http://localhost:9200
- POSTGRES_HOST=localhost
- ELASTIC_SEARCH_PASSWORD=1PxMVQIqGI7k7+CHaJNC

### Create .env File
- Create a file named .env in the root of your project to store environment variables.

#### .env
- SECRET_KEY_BASE=89a3e9dc9925a7e37e09b96aa44f0869
- POSTGRES_USER=postgres
- POSTGRES_DB=postgres
- POSTGRES_PASSWORD=promptfinder
- POSTGRES_HOST=192.168.32.3
- ELASTICSEARCH_URL=http://elasticsearch:9200

### Steps to Run Locally

1. **Clone the repository:**
   ```bash
   - git clone https://gitlab.com/test8480708/audemic_backend.git
   - cd your-project
3. **Go to Root project directory:**
    - cd prompt_engine 
4. **Install dependencies:**
    - bundle install
5. **Set up the database:**
    - rails db:create
    - rails db:migrate
    - rails db:seed
6. **Start Postgresql server:**
    - service postgresql start
7. **Start ElasticSearch server:**
    - systemctl start elasticsearch.service
8. **Start the Rails server:**
    - rails server
9. Open your browser and visit http://localhost:3000 to access the application.

10. **To Run test cases:**
    - rspec

### Running with Docker

1. docker compose up --build (for elasticsearch + postgres)
2. docker compose -f docker-compose-prompt.yaml up --build (for elastic-prompt-search)

    **#NOTE:**

    1. On the local machine, Elasticsearch and PostgreSQL servers must be stopped.
    2. Run both above commands for docker in two different terminals


### How to Use

1. Access the search page for prompt- https://audemic-app-new-48ec85db38a6.herokuapp.com
1. Search data like:
    - "a simple"
    - "A portrait"
    - "the persistence"
    - "Realistic car"
    - "Island inside of"

class RenameSearchTermToSearchKeywordInActivityLogs < ActiveRecord::Migration[7.1]
  def change
    rename_column :activity_logs, :search_term, :search_keyword
  end
end

class PromptsController < ApplicationController

  # If a search query is present, it performs an Elasticsearch search and displays the results.
  # GET /prompt/search
  def index
    # Assigning query param into another variable
    @search_query = params[:query]

    # Check if a search query is provided
    if @search_query.present?
      begin
        # Perform a search using Elasticsearch's search method with the provided query parameters
        prompts = Prompt.__elasticsearch__.search(params[:query]).response
        # Method is used to parse a JSON-formatted string
        parsed_json = JSON.parse(prompts.to_json)
        # Extract relevant data from the Elasticsearch response
        @prompts = parsed_json['hits']['hits'].map { |hit| hit['_source']['data'] }
        # Error handling for not founded prompt
        flash.now[:info] = "Please try with another keyword." unless @prompts.present?
      rescue Elasticsearch::Transport::Transport::Errors::NotFound
        # Handle error when the Elasticsearch index is not found
        flash.now[:alert] = 'Elasticsearch index not found. Please check your setup.'
      rescue Elasticsearch::Transport::Transport::Errors::RequestTimeout
        # Handle error when there's a timeout while making a request to Elasticsearch
        flash.now[:alert] = 'Elasticsearch request timed out. Please try again later.'
      rescue StandardError => e
        # Handle generic error
        flash.now[:alert] = "An error occurred: #{e.message}"
      end

      # This job is designed to perform asynchronous activity log creation
      LogCreationJob.perform_later
    end
  end
end
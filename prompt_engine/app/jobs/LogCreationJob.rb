class LogCreationJob < ApplicationJob
  queue_as :default
  
  # In this method, we create an ActivityLog record with the search keyword
  def perform
    # Create activity logs for the search operation
    ActivityLog.create(search_keyword: @search_query)
  end
end
  